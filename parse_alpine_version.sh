#!/bin/sh

sed '/^FROM/!d' Dockerfile | tail -n 1 | cut -d':' -f2

#!/bin/sh

if [ "$(uname -m)" == "x86_64" ]; then
    apk add --no-cache upx
    mv "/usr/local/bin/ezd_x86_64" "/usr/local/bin/ezd"
    rm "/usr/local/bin/ezd_aarch64"
elif [ "$(uname -m)" == "aarch64" ]; then
    mv "/usr/local/bin/ezd_aarch64" "/usr/local/bin/ezd"
    rm "/usr/local/bin/ezd_x86_64"
else
    printf "unsupported architecture!\n"
    exit 1
fi

apk add --no-cache build-base musl meson cmake pkgconfig git curl wget jq docker python3 perl curl-dev openssl-dev libpq-dev sqlite-dev zlib-dev

FROM registry.gitlab.com/sbenv/veroxis/ezd-rs:1.12.3 as ezd

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY --from=ezd /ezd_x86_64-unknown-linux-musl /usr/local/bin/ezd_x86_64
COPY --from=ezd /ezd_aarch64-unknown-linux-musl /usr/local/bin/ezd_aarch64

COPY install_deps.sh /install_deps.sh
RUN /install_deps.sh && rm /install_deps.sh
